﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class NucleonSpawner : MonoBehaviour
{
    public float timeBetweenSpawns;

    public float spawnDistance;

    public Nucleon nucleonsPref;

    private float timeSinceLastSpawn;

    public Material[] mats;

    public bool onPause = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void FixedUpdate()
    {
        timeSinceLastSpawn += Time.deltaTime;
        if (timeSinceLastSpawn >= timeBetweenSpawns)
        {
            timeSinceLastSpawn -= timeBetweenSpawns;
            SpawnNucleon();
        }
    }

    void SpawnNucleon()
    {
        Material mat = mats[Random.Range(0, mats.Length)];
        Nucleon spawn = Instantiate<Nucleon>(nucleonsPref);
        spawn.GetComponent<MeshRenderer>().material = mat;
        spawn.transform.localPosition = Random.onUnitSphere * spawnDistance;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
