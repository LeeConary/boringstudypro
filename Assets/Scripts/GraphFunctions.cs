﻿using UnityEngine;
public delegate Vector3 GraphFunction(float u, float v, float t);

public enum TransitionType
{
    Cycle,
    Random,
}
public enum GraphFunctionName
{
    Since, 
    MultiSince,
    Since3D,
    MultiSince3D,
    MainXZSince3D,
    Ripple,
    Cylinder,
    Sphere,
    Torus
}

public class GraphFunctions 
{
   
}
