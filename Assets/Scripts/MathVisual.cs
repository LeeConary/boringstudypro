﻿using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
using Vector3 = UnityEngine.Vector3;

public class MathVisual : MonoBehaviour
{
    public Transform cubePref;

    public GraphFunctionName functionName;
    public TransitionType transitionType;
    
    [SerializeField, Min(0f)] private float functionDuration = 1f, transitionDuration = 1f;
    private float duration;
    private bool transitioning = true;
    private GraphFunctionName transitionFunctionName;

    [Range(0, 100)] public int cubeCounts = 50;
    [Range(0,2)]public float zf = 1f;

    public static float staticZf;
    private List<GameObject> obList;
    static GraphFunction[] functions = { 
        SinceFunction , MutiSinceFunction, 
        Since3DFunction, MutiSince3DFunction,
        MainXZSince3DFunction,
        Ripple,Cylinder,Sphere,Torus
    };
    private const float pi = Mathf.PI;

    // Start is called before the first frame update
    // Source article url : https://mp.weixin.qq.com/s/X5-_hf546UofK9fAdjQl1g
    //Remeber make the x coordinate in [-1, 1]
    void Start()
    {
        obList = new List<GameObject>();
        GameObject parent = GameObject.Find("Graph");
        float steps = 2f / cubeCounts;
        for (int i = 0; i < cubeCounts * cubeCounts; i++)
        {
            GameObject cube = Instantiate(cubePref.gameObject);
            Transform cubeTrans = cube.transform;
            cubeTrans.SetParent(parent.transform);
            cubeTrans.localScale = Vector3.one * steps;
            obList.Add(cube);
        }
    }

    // Update is called once per frame
    void Update()
    {
        staticZf = zf;
        duration += Time.deltaTime;
        if (transitioning)
        {
            if (duration >= transitionDuration)
            {
                duration -= transitionDuration;
                transitioning = false;
            }
        }
        else if (duration >= functionDuration)
        {
            duration -= functionDuration;
            transitioning = true;
            transitionFunctionName = functionName;
            PickNextFunction();
        }

        if (transitioning)
        {
            UpdateFunctionTransition();
        }
        else
        {
            UpdateFunction();
        }
    }

    void PickNextFunction()
    {
        functionName = transitionType == TransitionType.Cycle ?
            GetNextFunctionName(functionName) :
            GetRandomFunctionNameOtherThan(functionName);
    }

    void UpdateFunction()
    {
        float time = Time.time;
        GraphFunction func = functions[(int)functionName];
        float steps = 2f / cubeCounts;
        for (int i = 0, z = 0; z < cubeCounts; z++)
        {
            float v = (z + 0.5f) * steps - 1f;
            for (int x = 0; x < cubeCounts; x++, i++)
            {
                float u = (x + 0.5f) * steps - 1f;
                obList[i].transform.localPosition = func(u, v, time);
            }
        }
    }

    void UpdateFunctionTransition()
    {
        float time = Time.time;
        //GraphFunction func = functions[(int)functionName];
        GraphFunction fromFunc = functions[(int) transitionFunctionName];
        GraphFunction toFunc = functions[(int) functionName];
        float progress = duration / transitionDuration;
        float steps = 2f / cubeCounts;
        for (int i = 0, z = 0; z < cubeCounts; z++)
        {
            float v = (z + 0.5f) * steps - 1f;
            for (int x = 0; x < cubeCounts; x++, i++)
            {
                float u = (x + 0.5f) * steps - 1f;
                obList[i].transform.localPosition = Morph(u, v, time, fromFunc, toFunc, progress);
            }
        }
    }

    public static Vector3 Morph(float u, float v, float t, GraphFunction from, GraphFunction to, float progress)
    {
        return Vector3.LerpUnclamped(from(u,v,t), to(u,v,t), Mathf.SmoothStep(0f, 1f, progress));
    }

    public static GraphFunctionName GetNextFunctionName(GraphFunctionName name)
    {
        return ((int)name < functions.Length - 1)? name + 1 : 0;
    }

    public static GraphFunctionName GetRandomFunctionNameOtherThan(GraphFunctionName name)
    {
        var choice = (GraphFunctionName) Random.Range(0, functions.Length);
        return choice == name ? 0 : choice;
    }

    static Vector3 Since3DFunction(float x, float z, float t)
    {
        Vector3 p;
        p.x = x;
        p.z = z;
        p.y = Mathf.Sin(pi * (x + z + t)) * staticZf;
        return p;
    }

    static Vector3 MutiSince3DFunction(float x, float z, float t)
    {
        Vector3 p;
        p.x = x;
        p.z = z;
        var func1 =  Mathf.Sin(pi * (x + z + t));
        var func2 = Mathf.Sin(pi * (z + t));
        p.y = (func1 + func2) * staticZf;
        return p;
    }

    static Vector3 MainXZSince3DFunction(float x, float z, float t)
    {
        Vector3 p;
        p.x = x;
        p.z = z;
        var mainFunc = 4f * Mathf.Sin(pi * (x + z + t * 0.5f));
        var xFunc = Mathf.Sin(pi * (x + t));
        var zFunc = Mathf.Sin(2f * pi * (z + 2f * t)) * 0.5f;
        float y = (mainFunc + xFunc + zFunc) / 5.5f;
        p.y = y * staticZf;
        return p;
    }

    static Vector3 SinceFunction(float x, float z, float t)
    {
        Vector3 p;
        p.x = x;
        p.z = z;
        p.y = Mathf.Sin(pi * (x + t)) * staticZf;
        return p;
    }

    static Vector3 MutiSinceFunction(float x, float z, float t)
    {
        Vector3 p;
        p.x = x;
        p.z = z;
        float y = 2f * Mathf.Sin(pi * (x + t));
        y += Mathf.Sin(2f * pi * (x + 2f * t)) / 2f;
        y *= 2f / 3f;
        p.y = y * staticZf;
        return p;
    }

    static Vector3 Ripple(float x, float z, float t)
    {
        Vector3 p;
        p.x = x;
        p.z = z;
        float d = Mathf.Sqrt(x * x + z * z);
        float y = Mathf.Sin(pi * (d * 4f - t));
        y /= 1 + 10 * d;
        p.y = y * staticZf;
        return p;
    }

    static Vector3 Cylinder(float u, float v, float t)
    {
        Vector3 p;
        float r = 0.8f + Mathf.Sin(pi * (6f *u + 2f * v + t)) * 0.2f;
        p.x = r * staticZf * Mathf.Sin(pi * u);
        p.y = v * staticZf;
        p.z = r * staticZf * Mathf.Cos(pi * u);
        return p;
    }

    static Vector3 Sphere(float u, float v, float t)
    {
        Vector3 p;
        float r = 0.8f + Mathf.Sin(pi * (6f * u + 2f * v + t)) * 0.1f;
        r += Mathf.Cos(pi * (4f * v + t)) * 0.1f;
        float s = r * Mathf.Cos(pi * 0.5f * v);
        p.x = s * staticZf * Mathf.Sin(pi * u);
        p.y = r * Mathf.Sin(pi * 0.5f * v);
        p.z = s * staticZf * Mathf.Cos(pi * u);
        return p;
    }

    static Vector3 Torus(float u, float v, float t)
    {
        Vector3 p;
        float r1 = 0.65f + Mathf.Sin(pi * (6f * u + t)) * 0.1f;
        float r2 = 0.2f + Mathf.Sin(pi * (4f * v + t)) * 0.1f;
        float s = r2 * Mathf.Cos(pi * v) + r1;
        p.x = s * staticZf * Mathf.Sin(pi * u);
        p.y = r2 * Mathf.Sin(pi * v);
        p.z = s * staticZf * Mathf.Cos(pi * u);
        return p;
    }
}
