﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fractal : MonoBehaviour
{
    public Mesh[] meshs;

    public Material material;

    public int maxDepth = 5;

    private int depth = 0;

    public float childScale;

    private Material[,] mats;

    public float spawnProbability;

    public float maxRotateSpeed;
    float rotateSpeed;
    public float maxRotateTwist;
    private float rotateTwist;

    private Vector3[] childDirections =
    {
        Vector3.up,
        Vector3.right,
        Vector3.left,
        Vector3.forward, 
        Vector3.back, 
    };

    private Quaternion[] rotations =
    {
        Quaternion.identity,
        Quaternion.Euler(new Vector3(0, 0, -90f)),
        Quaternion.Euler(new Vector3(0, 0, 90f)),
        Quaternion.Euler(new Vector3(90f, 0, 0)),
        Quaternion.Euler(new Vector3(-90f, 0, 0)),
    };
    // Start is called before the first frame update
    void Start()
    {
        rotateSpeed = Random.Range(-maxRotateSpeed, maxRotateSpeed);
        if (mats == null)
        {
            IntilizeMaterial();
        }
        gameObject.AddComponent<MeshFilter>().mesh = meshs[Random.Range(0,meshs.Length)];
        gameObject.AddComponent<MeshRenderer>().material = mats[depth, Random.Range(0,2)];
        transform.Rotate(Random.Range(-maxRotateTwist, maxRotateTwist), 0f, 0f);

        if (depth < maxDepth)
        {
            StartCoroutine(CreateChildren());
        }
    }

    void IntilizeMaterial()
    {
        mats = new Material[maxDepth + 1, 2];
        for (int i = 0; i <= maxDepth; i++)
        {
            float t = i / (maxDepth - 1f);
            t *= t;
            mats[i, 0] = new Material(material);
            mats[i, 0].color = Color.Lerp(Color.white, Color.yellow, t);
            mats[i, 1] = new Material(material);
            mats[i, 1].color = Color.Lerp(Color.white, Color.cyan, t);
        }
        mats[maxDepth, 0].color = Color.magenta;
        mats[maxDepth, 1].color = Color.red;
    }

    IEnumerator CreateChildren()
    {
        for (int i = 0; i < childDirections.Length; i++)
        {
            if (Random.value < spawnProbability)
            {
                yield return new WaitForSeconds(Random.Range(0.1f, 0.5f));
                Fractal childFractal = new GameObject("Fractal Child").AddComponent<Fractal>();
                childFractal.Intitialize(this, i);
            }
        }
    }

    void Intitialize(Fractal parent, int childIndex)
    {
        meshs = parent.meshs;
        mats = parent.mats;
        maxDepth = parent.maxDepth;
        childScale = parent.childScale;
        maxRotateSpeed = parent.maxRotateSpeed;
        spawnProbability = parent.spawnProbability;
        maxRotateTwist = parent.maxRotateTwist;
        transform.SetParent(parent.gameObject.transform);
        transform.localScale = Vector3.one * childScale;
        transform.localPosition = childDirections[childIndex] * (0.5f + 0.5f * childScale);
        depth = parent.depth + 1;
        transform.localRotation = rotations[childIndex];
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0f , rotateSpeed * Time.deltaTime, 0f);
    }
}
