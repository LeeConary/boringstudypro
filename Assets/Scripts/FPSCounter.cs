﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class FPSCounter : MonoBehaviour
{
    public int FPS { get; private set; }
    public int AverageFPS { get; private set; }
    public int LowFPS { get; private set; }
    public int HighFPS { get; private set; }
    public int frameRange = 60;

    private int[] fpsBufffer;

    private int fpsBufferIndex;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        if (fpsBufffer == null || fpsBufffer.Length != frameRange)
        {
            InitializeBuffer();;
        }
        UpdateBuffer();
        CaculateFPS();
    }

    void UpdateBuffer()
    {
        fpsBufffer[fpsBufferIndex++] = (int) (1f / Time.unscaledDeltaTime);
        if (fpsBufferIndex >= frameRange)
        {
            fpsBufferIndex = 0;
        }
    }

    void CaculateFPS()
    {
        int sum = 0;
        int lowest = int.MaxValue;
        int highest = 0;
        for (int i = 0; i < frameRange; i++)
        {
            int fps = fpsBufffer[i];
            sum += fpsBufffer[i];
            if (fps > highest)
            {
                highest = fps;
            }

            if (fps < lowest)
            {
                lowest = fps;
            }
        }

        AverageFPS = sum / frameRange;
        HighFPS = highest;
        LowFPS = lowest;
    }

    void InitializeBuffer()
    {
        if (frameRange <= 0)
        {
            frameRange = 1;
        }

        fpsBufffer = new int[frameRange];
        fpsBufferIndex = 0;
    }
}
