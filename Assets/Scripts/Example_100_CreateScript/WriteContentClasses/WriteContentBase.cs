using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;

namespace CreateScript
{
    internal abstract class WriteContentBase
    {
        public StringBuilder stringBuilder { get; private set; }
        protected WriteContentBase(StringBuilder strBuilder)
        {
            this.stringBuilder = stringBuilder;
        }

        protected void ParsingContent()
        {
            if (stringBuilder == null)
            {
                Debug.LogError("Try get content failure because content is null");
                return;
            }

            if (string.IsNullOrEmpty(stringBuilder.ToString()))
            {
                Debug.LogError("Content string is Empty. Please edit something");
                return;
            }
        }
    }
}
