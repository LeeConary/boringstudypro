using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text;
using System;

namespace CreateScript
{
    public class CreateScriptProcesser
    {
        string m_templateFile;
        Action<CreateScriptProcesser, StringBuilder> m_generatorAction;
        public CreateScriptProcesser()
        {
        }

        public void SetTemplateFile(string path, Encoding encodingType = null)
        {
            try
            {
                m_templateFile = File.ReadAllText(path, encodingType ?? Encoding.UTF8);
                Debug.Log(string.Format("Get File Template {0} Success", m_templateFile));
            }
            catch (Exception e)
            {
                Debug.LogError(string.Format("Get File Template {0} Fail", path));
                Debug.LogError(e);
            }
        }

        public void SetGenerator(Action<CreateScriptProcesser, StringBuilder> generatorAction)
        {
            m_generatorAction = generatorAction;
        }

        public bool GenerateCodeFile(string outPutFileName, Encoding encoding)
        {
            if (string.IsNullOrEmpty(m_templateFile))
            {
                Debug.LogError(string.Format("Cannot get template file {0}", m_templateFile));
                return false;
            }

            if (string.IsNullOrEmpty(outPutFileName))
            {
                Debug.LogError(string.Format("Cannont get outPutFileName, Are you sure to set outPutFileName while you try to create?"));
                return false;
            }

            try
            {
                StringBuilder stringBuilder = new StringBuilder(m_templateFile);
                m_generatorAction?.Invoke(this, stringBuilder);
                using (FileStream fileStream = new FileStream(outPutFileName, FileMode.Create, FileAccess.ReadWrite))
                {
                    using (StreamWriter stream = new StreamWriter(fileStream, encoding ?? Encoding.UTF8))
                    {
                        stream.Write(stringBuilder.ToString());
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                Debug.LogError(e);
                return false;
            }
        }
    }
}
