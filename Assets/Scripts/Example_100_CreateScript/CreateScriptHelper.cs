using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text;
using UnityEditor;

namespace CreateScript
{
    public class CreateScriptHelper
    {
        static CreateScriptHelper m_createScriptHelper;
        string templateFilePath = CreateScriptConst.templatePath;

        public static CreateScriptHelper GetCreateScriptHelper()
        {
            if (m_createScriptHelper == null)
            {
                m_createScriptHelper = new CreateScriptHelper();
            }
            return m_createScriptHelper;
        }
        public CreateScriptHelper()
        {
        }

        public void GenerateCodeFile(string outPutFile)
        {
            CreateScriptProcesser processer = new CreateScriptProcesser();
            processer.SetTemplateFile(templateFilePath);
            processer.SetGenerator(GenerateAction);
            string filePath = Path.Combine(CreateScriptConst.savePath, outPutFile + ".cs");
            if ((!processer.GenerateCodeFile(filePath, Encoding.UTF8)) && File.Exists(outPutFile))
            {
                File.Delete(outPutFile);
            }
#if UNITY_EDITOR
            AssetDatabase.Refresh();
#endif
        }

        private void GenerateAction(CreateScriptProcesser processer, StringBuilder strBuilder)
        {

        }
    }
}