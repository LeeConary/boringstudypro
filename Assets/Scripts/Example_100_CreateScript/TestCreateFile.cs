using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CreateScript;

public class TestCreateFile : MonoBehaviour
{
    CreateScriptHelper createScriptHelper;
    // Start is called before the first frame update
    void Start()
    {
        createScriptHelper = CreateScriptHelper.GetCreateScriptHelper();
        createScriptHelper.GenerateCodeFile("TestGenerate");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
