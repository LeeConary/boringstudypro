using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CreateScript
{
    public class CreateScriptConst
    {
        public static readonly string dataPath = Application.dataPath;
        public static readonly string templatePath = "Assets/Scripts/Example_100_CreateScript/TemplateFile/TestTemplate.txt";
        public static readonly string savePath = "Assets/Scripts/Example_100_CreateScript/SaveScriptFile";
    }
}