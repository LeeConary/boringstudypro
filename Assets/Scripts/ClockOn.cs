﻿using System;
using UnityEngine;

public class ClockOn : MonoBehaviour
{
    public Transform hour;

    public Transform minute;

    public Transform seconds;

    public bool isContineue;

    private const float secPerMin = 60f;
    const float timeDegree = 30f;

    const float hourAndSecondDegree = 6f;
    // Start is called before the first frame update
    void Start()
    {
        SetTime();
    }

    // Update is called once per frame
    void Update()
    {
        if (isContineue)
        {
            SetTimeContineue();
        }
        else
        {
            SetTime();
        }
    }

    void SetTimeContineue()
    {
        TimeSpan time = DateTime.Now.TimeOfDay;
        float minNum = (float)time.TotalMinutes * hourAndSecondDegree;
        float secNum = (float)time.TotalSeconds * hourAndSecondDegree;
        float hourNum = (float)(time.TotalHours * timeDegree + time.TotalMinutes * (timeDegree / (secPerMin * 1000f)));
        hour.localRotation = Quaternion.Euler(0f, hourNum, 0f);
        minute.localRotation = Quaternion.Euler(0f, minNum, 0f);
        seconds.localRotation = Quaternion.Euler(0f, secNum, 0f);
    }

    void SetTime()
    {
        DateTime time = DateTime.Now;
        float minNum = (float)time.Minute * hourAndSecondDegree;
        float secNum = (float)time.Second * hourAndSecondDegree;
        float hourNum = (float)(time.Hour * timeDegree + time.Minute * (timeDegree / secPerMin));
        hour.localRotation = Quaternion.Euler(0f, hourNum, 0f);
        minute.localRotation = Quaternion.Euler(0f, minNum, 0f);
        seconds.localRotation = Quaternion.Euler(0f, secNum, 0f);
    }
}
