﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateTest : MonoBehaviour
{
    public GameObject target;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //transform.localRotation = Quaternion.Euler(new Vector3(0, 20 * Time.deltaTime, 0)) * transform.localRotation;
        //transform.rotation = Quaternion.AngleAxis(20 * Time.deltaTime, transform.up) * transform.rotation;

        Vector3 targetPos = target.transform.position;
        Vector3 objPos = transform.position;
        Vector3 lookDirection = new Vector3(targetPos.x - objPos.x, 0, targetPos.z - objPos.z);
        Quaternion destinaRotation = Quaternion.LookRotation(lookDirection, transform.up);
        transform.rotation = Quaternion.Lerp(transform.rotation, destinaRotation, 1f);
    }
}
