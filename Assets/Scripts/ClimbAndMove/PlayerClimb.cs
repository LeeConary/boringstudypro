﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.ProBuilder.MeshOperations;

public class PlayerClimb : MonoBehaviour
{
    public Animator animator;

    protected bool onWall = false;

    protected bool canClimb = false;

    public float wallOffset = 1f;
    public float canClimbDistance = 1f;

    private SkinnedMeshRenderer meshRenderer;
    private Vector3 climbTargetPos;

    private Quaternion climbTargetRotation;
    // Start is called before the first frame update
    void Start()
    {
        CheckCanClimb();
    }

    void CheckCanClimb()
    {
        Vector3 origin = transform.position;
        Vector3 direction = transform.forward;
        RaycastHit ray;
        Debug.DrawRay(origin, direction, Color.green);
        if (Physics.Raycast(origin, direction, out ray, canClimbDistance))
        {
            InitClimb(ray);
        }
        else
        {
            onWall = false;
            canClimb = false;
            animator.Play("idle");
        }
    }

    void InitClimb(RaycastHit ray)
    {
        Debug.DrawRay(ray.point, ray.normal, Color.red, 66);
        climbTargetPos = ray.point + ray.normal * wallOffset;
        //var rotaVec3 = new Vector3(transform.rotation.x, transform.rotation.y, ray.normal.z);
        //climbTargetRotation = Quaternion.Euler(rotaVec3);
        if (!canClimb && !onWall)
        {
            canClimb = true;
            onWall = false;
            animator.CrossFade("Climbing", 0.2f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        CheckCanClimb();
        if (Vector3.Distance(transform.position, climbTargetPos) <= 0.01f)
        {
            onWall = true;
            transform.position = climbTargetPos;
           //transform.rotation = climbTargetRotation;
        }
        else if (canClimb && !onWall)
        {
            Vector3 climbPos = Vector3.MoveTowards(transform.position, climbTargetPos, 0.5f);
            //Quaternion rotation = Quaternion.Lerp(transform.rotation, climbTargetRotation, 0.1f);
            transform.position = climbPos;
            //transform.rotation = rotation;
        }
    }
}
