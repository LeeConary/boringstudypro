﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //Components
    private PlayerStateMachine pMachine;
    private InputController inputCtrl;
    private CharacterController charCtrl;
    private CameraController cameraCtrl;

    //Public Properties
    public float moveSpeed = 10f;
    public float accertation = 2f;
    public float maxTurnSpeed = 1200f;
    public float minTurnSpeed = 500f;
    public float gravity = 10f;
    public float jumpSpeed = 10f;
    public float groundHitDis = 1f;

    //Private Properties
    private float f_desireVelocity;
    private float f_currentVelocity = 0f;
    private float verticalSpeed;

    //Const Properties
    private const float const_jumpDeclarationSpeed = 10f;
    private const float const_stickingGroundGravityProportion = 0.3f;
    private const float const_GroundedRayDistance = 1f;

    //States
    private bool isGrounded = true;
    private bool isReadyJump;

    // Start is called before the first frame update
    void Awake()
    {
        pMachine = GetComponent<PlayerStateMachine>();
        if (pMachine == null)
        {
            pMachine = gameObject.AddComponent<PlayerStateMachine>();
        }

        inputCtrl = GetComponent<InputController>();
        charCtrl = GetComponent<CharacterController>();
        cameraCtrl = FindObjectOfType<CameraController>();
        if (cameraCtrl.followTarget == null)
        {
            cameraCtrl.followTarget = gameObject;
        }

        if (cameraCtrl.lookAtTarget == null)
        {
            cameraCtrl.lookAtTarget = gameObject;
        }
    }

    void OnEnabled()
    {
       
    }

    public void PlayerMove()
    {
        SetVerticalMovement();
        SetHorizontalMovement();
        TransPlayerRotation();

        UpdatePlayerMoveAction();
    }

    void UpdatePlayerMoveAction()
    {
        Vector3 movement = Vector3.zero;
        Vector3 moveDirection = transform.forward;

        movement = f_currentVelocity * moveDirection * Time.deltaTime;
        movement += verticalSpeed * Vector3.up * Time.deltaTime;
        charCtrl.Move(movement);
        isGrounded = charCtrl.isGrounded;
    }

    void SetHorizontalMovement()
    {
        Vector3 inputValue = inputCtrl.input.moveInput;
        float isMove = inputValue.sqrMagnitude > 0? 1:0;

        f_desireVelocity = (transform.forward * moveSpeed * isMove).magnitude;
        f_currentVelocity = Mathf.MoveTowards(f_currentVelocity, f_desireVelocity, accertation * Time.deltaTime);
    }

    void SetVerticalMovement()
    {
        if (isGrounded && !inputCtrl.input.jumpInput)
        {
            isReadyJump = true;
        }

        if (isGrounded)
        {
            verticalSpeed = -gravity * const_stickingGroundGravityProportion;
            if (inputCtrl.input.jumpInput && isReadyJump)
            {
                verticalSpeed = jumpSpeed;
                isGrounded = false;
                isReadyJump = false;
            }
        }
        else
        {
            if (!inputCtrl.input.jumpInput && verticalSpeed > 0f)
            {
                verticalSpeed -= const_jumpDeclarationSpeed * Time.deltaTime;
            }

            if (Mathf.Approximately(verticalSpeed, 0f))
            {
                verticalSpeed = 0f;
            }

            verticalSpeed -= gravity * Time.deltaTime;
        }
    }

    private Vector3 cacheInput = Vector3.zero;
    Vector3 SetTargetRotation()
    {
        Vector3 moveInput = inputCtrl.input.moveInput;
        cacheInput = moveInput.normalized == Vector3.zero ? cacheInput : moveInput;
        Vector3 localMoveDirection = cacheInput.normalized;
        Vector3 cameraLookDirection = cameraCtrl.CameraDirection();

        Vector3 forward = Quaternion.Euler(0f, cameraLookDirection.y, 0f) * Vector3.forward;
        forward.y = 0f;
        forward.Normalize();

        Quaternion targetQuaternion;
        if (Mathf.Approximately(Vector3.Dot(localMoveDirection, Vector3.forward), -1.0f))
        {
            targetQuaternion = Quaternion.LookRotation(-forward);
        }
        else
        {
            Quaternion cameraToInputOffset = Quaternion.FromToRotation(Vector3.forward, localMoveDirection);
            targetQuaternion = Quaternion.LookRotation(cameraToInputOffset * forward);
        }

        Vector3 resultForward = targetQuaternion * Vector3.forward;
        return resultForward;
    }

    void TransPlayerRotation()
    {
        if (inputCtrl.input.moveInput != Vector3.zero)
        {
            var targetRotation = Quaternion.LookRotation(SetTargetRotation());
            float duration = Mathf.Min(1, f_currentVelocity);
            float groundedTurnSpeed = Mathf.Lerp(maxTurnSpeed, minTurnSpeed, duration / moveSpeed);
            transform.rotation =
                Quaternion.RotateTowards(transform.rotation, targetRotation, groundedTurnSpeed * Time.deltaTime);
        }
    }
}
