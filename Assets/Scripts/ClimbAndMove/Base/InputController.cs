﻿using System;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public struct PlayInput
    {
        public Vector3 moveInput;
        public Vector2 mouseInput;
        public bool jumpInput;
    }

    private static PlayInput _input;
    private Enum _state;
    public PlayInput input
    {
        get
        {
            return _input;
        }
    }

    public Enum state
    {
        get { return _state; }
    }

    // Start is called before the first frame update
    void Start()
    {
        _input = new PlayInput();
    }

    // Update is called once per frame
    void Update()
    {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");
        
        Vector3 moveInput = new Vector3(h, 0, v);
        Vector2 mouseInput = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

        float jumpOn = Input.GetAxisRaw("Jump");
        bool isJump = Math.Abs(jumpOn) > 0;

        _input.moveInput = moveInput;
        _input.mouseInput = mouseInput;
        _input.jumpInput = isJump;
    }
}
