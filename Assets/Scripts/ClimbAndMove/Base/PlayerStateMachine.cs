﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerState
{
    Idle,
    Walk,
    Jump,
}

public class PlayerStateMachine : BaseStateMachine
{
    private InputController inputCtrl;
    private PlayerController pCtrl;
    
    void Awake()
    {
        inputCtrl = GetComponent<InputController>();
        if (inputCtrl == null)
        {
            inputCtrl = gameObject.AddComponent<InputController>();
        }
        pCtrl = GetComponent<PlayerController>();
    }

    private void OnEnable()
    {
        currentState = PlayerState.Idle;
    }

    void FixedUpdate()
    {
        MachineUpdate();
    }

    void Idle_EnterState()
    {
        //print("Idle_EnterState()");
    }

    void Idle_StateUpdate()
    {
        //print("Idle_StateUpdate()");
        pCtrl.PlayerMove();
        if (inputCtrl.input.moveInput != Vector3.zero)
        {
            currentState = PlayerState.Walk;
        }
    }

    void Idle_ExitState()
    {
        //print("Idle_ExitState()");
    }

    void Jump_EnterState()
    {
        //print("Jump_EnterState()");
    }

    void Walk_EnterState()
    {
       //print("Walk_EnterState()");
    }

    void Walk_StateUpdate()
    {
        //print("Walk_StateUpdate()");
        pCtrl.PlayerMove();
        if (inputCtrl.input.moveInput == Vector3.zero)
        {
            currentState = PlayerState.Idle;
        }
    }

    void Walk_ExitState()
    {
        //print("Walk_ExitState()");
    }
}
