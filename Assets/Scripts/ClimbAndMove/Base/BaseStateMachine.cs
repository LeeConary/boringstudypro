﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class BaseStateMachine : MonoBehaviour
{

    protected float timeEnteredState;

    public class State
    {
        public Action enterState = DoNothing;
        public Action stateUpdate = DoNothing;
        public Action exitState = DoNothing;

        public Enum currentState;
    }

    [HideInInspector] public Enum lastState;

    private State state = new State();

    public Enum currentState
    {
        get { return state.currentState; }
        set
        {
            if (state.currentState == value)
            {
                return;
            }

            ChangingState();
            state.currentState = value;
            ConfigureCurrentState();
        }
    }

    void ChangingState()
    {
        lastState = state.currentState;
        timeEnteredState = Time.time;
    }

    void ConfigureCurrentState()
    {
        state.exitState?.Invoke();

        state.enterState = SetAction("EnterState");
        state.stateUpdate = SetAction("StateUpdate");
        state.exitState = SetAction("ExitState");

        state.enterState?.Invoke();
    }

    private Dictionary<Enum, Dictionary<string, Action>> cacheActions = new Dictionary<Enum, Dictionary<string, Action>>();
    Action SetAction(string methodRoot)
    {
        Dictionary<string, Action> lookUp;
        if (!cacheActions.TryGetValue(state.currentState, out lookUp))
        {
            lookUp = new Dictionary<string, Action>();
        }

        Action returnValue;
        if (!lookUp.TryGetValue(methodRoot, out returnValue))
        {
            var methodInfo = GetType().GetMethod(state.currentState + "_" + methodRoot, System.Reflection.BindingFlags.Instance
                                                             | System.Reflection.BindingFlags.Public |
                                                             System.Reflection.BindingFlags.NonPublic |
                                                             System.Reflection.BindingFlags.InvokeMethod);
            if (methodInfo != null)
            {
                returnValue = Delegate.CreateDelegate(typeof(Action), this, methodInfo) as Action;
            }
            else
            {
                returnValue = DoNothing;
            }

            lookUp[methodRoot] = returnValue;
        }

        return returnValue;
    }

    protected void MachineUpdate()
    {
        EarlyGlobalUpdate();

        state.stateUpdate();

        LateGlobalUpdate();
    }

    protected virtual void EarlyGlobalUpdate() { }

    protected virtual void LateGlobalUpdate() { }

    static void DoNothing()
    {

    }
}
