﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject lookAtTarget;
    public GameObject followTarget;
    public float Distance = 5f;
    public float Height = 2f;

    private Vector3 up;

    private Vector3 lookDirection;

    private Transform targetTrans;
    private Transform followTrans;

    private InputController inputCtrl;

    private float yRotation;
    // Start is called before the first frame update
    void Start()
    {
        targetTrans = lookAtTarget.transform;
        followTrans = followTarget.transform;

        up = targetTrans.up;
        lookDirection = targetTrans.forward;

        inputCtrl = followTarget.GetComponent<InputController>();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.position = followTrans.position;

        yRotation += inputCtrl.input.mouseInput.y;

        lookDirection = Quaternion.AngleAxis(inputCtrl.input.mouseInput.x, up) * lookDirection;
        Vector3 left = Vector3.Cross(lookDirection, up);
        transform.rotation = Quaternion.LookRotation(lookDirection, up);
        transform.rotation = Quaternion.AngleAxis(yRotation, left) * transform.rotation;

        transform.position -= transform.forward * Distance;
        transform.position += up * Height;
    }

    public Vector3 CameraDirection()
    {
        return transform.eulerAngles;
    }
}
