﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointDirector : MonoBehaviour
{
    public GameObject targetObj;
    RectTransform pointRectTrans;

    [Range(0, 1)]
    public float longAxisPercent;
    [Range(0, 1)]
    public float minorAxisPercent; 

    CanvasScaler cs;
    // Start is called before the first frame update
    void Start()
    {
        cs = GameObject.Find("Canvas").GetComponent<CanvasScaler>();
        pointRectTrans = GetComponent<RectTransform>();
    }

    void UpdatePointScreenPos()
    {
        Vector3 viewPos = Camera.main.WorldToViewportPoint(targetObj.transform.position);
        Debug.Log(viewPos);
        Vector2 screenSize = cs.referenceResolution;
        Vector2 pointPosInView = new Vector2(screenSize.x * viewPos.x, screenSize.y * viewPos.y);

        float screenHalfX = screenSize.x / 2;
        float screenHalfY = screenSize.y / 2;

        Vector2 centerAnchoredPos = new Vector2(pointPosInView.x - screenHalfX, pointPosInView.y - screenHalfY);
        
        float a = longAxisPercent * screenHalfX;
        float b = minorAxisPercent * screenHalfY;

        float result = (Mathf.Pow(centerAnchoredPos.x, 2) / Mathf.Pow(a, 2)) + (Mathf.Pow(centerAnchoredPos.y, 2) / Mathf.Pow(b, 2));
        if (result < 1 && viewPos.z > 0)
        {
            pointRectTrans.anchoredPosition = centerAnchoredPos;
        }
        else
        {
            Vector3 cameraTargetPos = Camera.main.transform.InverseTransformVector(targetObj.transform.position);
            Vector2 desDir = new Vector2(cameraTargetPos.x, cameraTargetPos.y);
            desDir.Normalize();
            float k = desDir.y / desDir.x;
            float x = Mathf.Sign(desDir.x) * Mathf.Sqrt((a * a * b * b) / (b * b + a * a * k * k));
            float y = k * x;
            pointRectTrans.anchoredPosition = Vector2.Lerp(pointRectTrans.anchoredPosition, new Vector2(x, (viewPos.z > 0 ? 1 : -1) * y), 1);
        }
    }

    // Update is called once per frame
    void Update()
    {
        UpdatePointScreenPos();
    }
}
