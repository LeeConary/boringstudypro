﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dot : MonoBehaviour
{
    public GameObject target;
    Vector3 positionVec;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        positionVec = target.transform.position - transform.position;
    }

    [ContextMenu("Caculate Dot Result")]
    void JudgeObjDirections()
    {
        Debug.Log(Vector3.Dot(transform.forward, positionVec));
    }

    [ContextMenu("Caculate Angel")]
    void CaculateAngle()
    {
        Debug.Log(Vector3.Angle(transform.forward, positionVec));
    }

    [ContextMenu("Get Close Obj")]
    void GetCloseObj()
    {

    }
}
