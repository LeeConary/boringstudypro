﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GameDataWriter
{
    private BinaryWriter bwWriter;

    public GameDataWriter(BinaryWriter writer)
    {
        this.bwWriter = writer;
    }

    public void Write(float num)
    {
        bwWriter.Write(num);
    }

    public void Write(int num)
    {
        bwWriter.Write(num);
    }

    public void Write(Vector3 vec)
    {
        bwWriter.Write(vec.x);
        bwWriter.Write(vec.y);
        bwWriter.Write(vec.z);
    }

    public void Write(Quaternion quat)
    {
        bwWriter.Write(quat.x);
        bwWriter.Write(quat.y);
        bwWriter.Write(quat.z);
        bwWriter.Write(quat.w);
    }

    public void Write(Color color)
    {
        bwWriter.Write(color.r);
        bwWriter.Write(color.g);
        bwWriter.Write(color.b);
        bwWriter.Write(color.a);
    }
}
