﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game : PersistableObject
{
    // Start is called before the first frame update
    public KeyCode createKey = KeyCode.C;
    public KeyCode destroyKey = KeyCode.X;
    public KeyCode beginKey = KeyCode.R;
    public KeyCode saveKey = KeyCode.S;
    public KeyCode loadKey = KeyCode.L;
    
    public PersistentStorage storage;
    public ShapeFactory factory;
    public float creationSpeed { get; set; }
    public float destructionSpeed { get; set; }

    private float creationProgress;
    private float destructionProgress;

    private List<Shape> objectList;
    private const int saveVersion = 1;
    
    private void Awake()
    {
        objectList = new List<Shape>();
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(createKey)) CreateObject();

        if (Input.GetKey(destroyKey)) DestroyRandom();

            if (Input.GetKeyDown(beginKey)) Reset();

        if (Input.GetKeyDown(saveKey))
        {
            storage.Save(this, saveVersion);
        };

        if (Input.GetKeyDown(loadKey))
        {
            Reset();
            storage.Load(this);
        };

        creationProgress += Time.deltaTime * creationSpeed;
        if (creationProgress >= 1f)
        {
            creationProgress = 0f;
            CreateObject();
        }

        destructionProgress += Time.deltaTime * destructionSpeed;
        if (destructionProgress >= 1f)
        {
            destructionProgress = 0f;
            DestroyRandom();
        }
    }

    void CreateObject()
    {
        Shape obj = factory.GetRandom();
        var t = obj.transform;
        t.localPosition = Random.insideUnitSphere * 5f;
        t.localRotation = Random.rotation;
        t.localScale = Vector3.one * Random.Range(0.1f, 2f);
        obj.SetColor(Random.ColorHSV(0f,1f, 0.5f,1f,0.25f,1f,1f,1f));
        objectList.Add(obj);
    }

    void DestroyRandom()
    {
        if (objectList.Count > 0)
        {
            int index = Random.Range(0, objectList.Count);
            factory.Reclaim(objectList[index]);
            //List类用数组实现，因此在移除一个元素后数组之后的元素都需要移动
            //这里我们并不关心元素的顺序
            //因此可以从代码上取巧
            //objectList.RemoveAt(index);
            int lastIndex = objectList.Count - 1;
            objectList[index] = objectList[lastIndex];
            objectList.RemoveAt(lastIndex);
        }
    }

    void Reset()
    {
        for (int i = 0; i < objectList.Count; i++)
        {
            factory.Reclaim(objectList[i]);
        }
        objectList.Clear();
    }

    public override void Save(GameDataWriter writer)
    {
        writer.Write(objectList.Count);
        for (int i = 0; i < objectList.Count; i++)
        {
            writer.Write(objectList[i].ShapeId);
            writer.Write(objectList[i].MaterialId);
            objectList[i].Save(writer);
        }
    }

    public override void Load(GameDataReader reader)
    {
        int version = reader.Version;
        if (version > saveVersion)
        {
            Debug.LogError("Unsupport future save version " + version);
            return;
        }
        int count =reader.ReadInt();
        for (int i = 0; i < count; i++)
        {
            int shapeId = version > 0 ? reader.ReadInt() : 0;
            int matId = version > 0 ? reader.ReadInt() : 0;
            Shape obj = factory.Get(shapeId, matId);
            obj.Load(reader);
            objectList.Add(obj);
        }
    }
}
