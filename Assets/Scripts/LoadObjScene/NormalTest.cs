﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalTest : MonoBehaviour
{
    public GameObject target;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var direValue = Vector3.Dot(target.transform.position, transform.position);
        if (direValue < 1)
        {
            transform.LookAt(target.transform);
        }
        else
        {
            transform.localEulerAngles = Vector3.forward;
        }
    }
}
