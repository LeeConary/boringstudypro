﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class PersistentStorage : MonoBehaviour
{
    private string savePath;

    void Awake()
    {
        savePath = Path.Combine(Application.persistentDataPath, "saveFile");
    }

    public void Save(PersistableObject obj, int version)
    {
        using (var bwWriter = new BinaryWriter(File.Open(savePath, FileMode.Create)))
        {
            Debug.Log("save file at " + savePath);
            Debug.Log("save version : " + version);
            bwWriter.Write(-version);
            obj.Save(new GameDataWriter(bwWriter));
        }
    }

    public void Load(PersistableObject obj)
    {
        using (BinaryReader bwReader = new BinaryReader(File.Open(savePath, FileMode.Open)))
        {
            Debug.Log("load file at " + savePath);
            int readVersion = -bwReader.ReadInt32();
            Debug.Log("load version : " + readVersion);
            obj.Load(new GameDataReader(bwReader, readVersion));
        }
    }
}
