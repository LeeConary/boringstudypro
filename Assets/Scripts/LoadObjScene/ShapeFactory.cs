﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[CreateAssetMenu]
public class ShapeFactory : ScriptableObject
{
    [SerializeField] private Shape[] shapes;
    [SerializeField] private Material[] materials;
    [SerializeField] private bool Recycle;
    [SerializeField] private string poolSceneName;

    private List<Shape>[] shapePool;
    private Scene scenePool;

    public Shape Get(int shapeId = 0, int matId = 0)
    {
        Shape instance = null;
        if (Recycle)
        {
            if (shapePool == null)
            {
                CreatePools();
            }

            List<Shape> pool = shapePool[shapeId];
            int lastIndex = pool.Count - 1;
            if (lastIndex >= 0)
            {
                instance = pool[lastIndex];
                instance.gameObject.SetActive(true);
                pool.RemoveAt(lastIndex);
            }
            else
            {
                instance = Instantiate(shapes[shapeId]);
                instance.ShapeId = shapeId;
            }
        }
        else
        {
            instance = Instantiate(shapes[shapeId]);
        }
        return instance;
    }

    public Shape GetRandom()
    {
        int id = Random.Range(0, shapes.Length);
        int matId = Random.Range(0, materials.Length);
        return Get(id, matId);
    }

    //创建回收对象的对象池
    void CreatePools()
    {
        shapePool = new List<Shape>[shapes.Length];
        for (int i = 0; i < shapePool.Length; i++)
        {
            shapePool[i] = new List<Shape>();
        }
        //回收对象的场景
        //如果场景进行重新编译的时候，对象列表可能会丢失
        //因此需要将池场景中的对象重新加入到对象池列表中
        scenePool = SceneManager.GetSceneByName(poolSceneName);
        if (scenePool.isLoaded)
        {
            GameObject[] poolObjects = scenePool.GetRootGameObjects();
            for (int i = 0; i < poolObjects.Length; i++)
            {
                Shape pooledShape = poolObjects[i].GetComponent<Shape>();
                if (!pooledShape.gameObject.activeSelf)
                {
                    shapePool[pooledShape.ShapeId].Add(pooledShape);
                }
            }
            return;
        }
        // 如果没有回收场景就创建
        scenePool = SceneManager.CreateScene(poolSceneName);
    }

    public void Reclaim(Shape shapeToRecycle)
    {
        if (Recycle)
        {
            if (shapePool == null)
            {
                CreatePools();
            }
            shapePool[shapeToRecycle.ShapeId].Add(shapeToRecycle);
            shapeToRecycle.gameObject.SetActive((false));
            SceneManager.MoveGameObjectToScene(shapeToRecycle.gameObject, scenePool);
        }
        else
        {
            Destroy(shapeToRecycle.gameObject);
        }
    }
}
