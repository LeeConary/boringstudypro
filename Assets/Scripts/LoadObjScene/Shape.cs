﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shape : PersistableObject
{
    private int shapeId = int.MinValue;

    private MeshRenderer meshRenderer;

    private void Awake()
    {
        meshRenderer = GetComponent<MeshRenderer>();
    }

    public int ShapeId
    {
        get { return shapeId; }
        set
        {
            if (shapeId == int.MinValue && value != int.MinValue)
            {
                shapeId = value;
            }
            else
            {
                Debug.LogError("Not allowed to change shapeId");
            }
        }
    }

    public int MaterialId { get; private set; }
    public Color color { get; private set; }

    public void SetMaterial(Material material, int matId)
    {
        meshRenderer.material = material;
        MaterialId = matId;
    }

    //获得一次材质的属性块的标识符，使对应标识符的字段的值可以被改变
    //并让它在每个会话中保持不变
    private static readonly int colorProId = Shader.PropertyToID("_Color");
    static MaterialPropertyBlock propertyBlock;

    public void SetColor(Color color)
    {
        //meshRenderer.material.color = color;
        //在用上面代码设置颜色的时候会重新创建一个材质
        //因此可以用属性块来避免这种情况
        this.color = color;
        //Debug.Log(color);
        if (propertyBlock == null)
        {
            propertyBlock = new MaterialPropertyBlock();
        }
        propertyBlock.SetColor(colorProId, color);
        meshRenderer.SetPropertyBlock(propertyBlock);
    }

    public override void Save(GameDataWriter writer)
    {
        base.Save(writer);
        writer.Write(color);
    }

    public override void Load(GameDataReader reader)
    {
        base.Load(reader);
        SetColor(reader.Version > 0? reader.ReadColor() : Color.white);
    }
}
