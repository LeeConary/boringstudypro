﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GameDataReader
{
    private BinaryReader brReader;
    public int Version { get; }
    public GameDataReader(BinaryReader reader)
    {
        this.brReader = reader;
    }

    public GameDataReader(BinaryReader reader, int version)
    {
        this.brReader = reader;
        this.Version = version;
    }

    public float ReadFloat()
    {
        return brReader.ReadSingle();
    }

    public int ReadInt()
    {
        return brReader.ReadInt32();
    }

    public int ReadInt32()
    {
        return brReader.ReadInt32();
    }

    public Vector3 ReadVector3()
    {
        Vector3 value;
        value.x = brReader.ReadSingle();
        value.y = brReader.ReadSingle();
        value.z = brReader.ReadSingle();
        return value;
    }

    public Quaternion ReadQuaternion()
    {
        Quaternion value;
        value.x = brReader.ReadSingle();
        value.y = brReader.ReadSingle();
        value.z = brReader.ReadSingle();
        value.w = brReader.ReadSingle();
        return value;
    }

    public Color ReadColor()
    {
        Color value;
        value.r = brReader.ReadSingle();
        value.g = brReader.ReadSingle();
        value.b = brReader.ReadSingle();
        value.a = brReader.ReadSingle();
        return value;
    }
}
