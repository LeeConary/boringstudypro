﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;

public class MovingSpherePhysics : MonoBehaviour
{
    private Vector3 velocity, desireVelocity;
    private bool onGround;

    [SerializeField, Range(0f, 10f), Tooltip("移动的加速度")]
    private float maxAcceleration = 1f;

    [SerializeField, Range(0f, 10f), Tooltip("移动的加速度")]
    private float maxSpeed = 1f;

    [SerializeField, Range(0f, 10f), Tooltip("直接设置velocity的跳跃有问题，看看后续有啥解决方案")]
    private float jumpHeight = 2f;

    private Rigidbody body;
    private bool desireJump;
    private void Awake()
    {
        body = GetComponent<Rigidbody>();
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    Vector3 playerInput;

    void GetPlayerKeys()
    {
        playerInput.x = Input.GetAxis("Horizontal");
        playerInput.z = Input.GetAxis("Vertical");
        playerInput = Vector3.ClampMagnitude(playerInput, 1f);

        //或运算 一旦设置为true时就相当于 true | false。因此即使在下一帧中返回的是false，那么它总会取原来的值
        //除非在后面显式将其设置回false
        desireJump |= Input.GetButtonDown("Jump"); 
        desireVelocity = new Vector3(playerInput.x, 0f, playerInput.z) * maxSpeed;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        GetPlayerKeys();
        if (desireJump)
        {
            desireJump = false;
            Jump();
        }

        float maxSpeedChange = maxAcceleration * Time.deltaTime;
        velocity.x = Mathf.MoveTowards(velocity.x, desireVelocity.x, maxSpeedChange);
        velocity.z = Mathf.MoveTowards(velocity.z, desireVelocity.z, maxSpeedChange);
        body.velocity = velocity;
    }

    void Jump()
    {
        if (onGround)
        {
            velocity.y += Mathf.Sqrt(-2 * Physics.gravity.y * jumpHeight);
            StartCoroutine("JumpDelay");
        }
    }

    IEnumerator JumpDelay()
    {
        yield return  new WaitForSeconds(0.5f);
        velocity.y = -9.8f;
    }

    private void OnCollisionStay(Collision collision)
    {
        onGround = true;
        velocity.y = 0;
    }

    private void OnCollisionExit(Collision collision)
    {
        onGround = false;
    }

}
