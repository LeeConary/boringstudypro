﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingSphere : MonoBehaviour
{
    private Vector3 velocity;

    [SerializeField, Range(0f, 2f), Tooltip("移动的加速度")]
    private float maxAcceleration = 10f;

    [SerializeField, Range(0f, 2f), Tooltip("移动的加速度")]
    private float maxSpeed = 10f;

    [SerializeField, Tooltip("边缘")]
    Rect allowedArea = new Rect(-5, -5, 10, 10);
    [SerializeField, Range(0f, 1f), Tooltip("弹性值")]
    private float bouncingness = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    Vector3 playerInput;

    private Vector3 baseMove;
    // Update is called once per frame
    void FixedUpdate()
    {
        playerInput.x = Input.GetAxis("Horizontal");
        playerInput.z = Input.GetAxis("Vertical");
        playerInput = Vector3.ClampMagnitude(playerInput, 1f);
        //Debug.Log(playerInput);
        Vector3 desireVelocity = new Vector3(playerInput.x, 0f ,playerInput.z) * maxSpeed;
        float maxSpeedChange = maxAcceleration * Time.deltaTime;
        //if (velocity.x < desireVelocity.x)
        //{
        //    velocity.x = Mathf.Min(velocity.x + maxSpeedChange, desireVelocity.x);
        //}
        //else if (velocity.x > desireVelocity.x)
        //{
        //    velocity.x = Mathf.Max(velocity.x - maxSpeedChange, desireVelocity.x);
        //}
        velocity.x = Mathf.MoveTowards(velocity.x, desireVelocity.x, maxSpeedChange);
        velocity.z = Mathf.MoveTowards(velocity.z, desireVelocity.z, maxSpeedChange);
        Vector3 displacement = new Vector3(velocity.x, 0f, velocity.z);
        Vector3 newPosition = transform.localPosition + displacement;
        //if (!allowedArea.Contains(new Vector2(newPosition.x, newPosition.z)))
        //{
        //    newPosition.x = Mathf.Clamp(newPosition.x, allowedArea.xMin, allowedArea.xMax);
        //    newPosition.z = Mathf.Clamp(newPosition.z, allowedArea.yMin, allowedArea.yMax);
        //}
        if (newPosition.x < allowedArea.xMin)
        {
            newPosition.x = allowedArea.xMin;
            velocity.x = -velocity.x * bouncingness;
        }
        else if (newPosition.x > allowedArea.xMax)
        {
            newPosition.x = allowedArea.xMax;
            velocity.x = -velocity.x * bouncingness;
        }
        if (newPosition.z < allowedArea.yMin)
        {
            newPosition.z = allowedArea.yMin;
            velocity.z = -velocity.z * bouncingness;
        }
        else if (newPosition.z > allowedArea.yMax)
        {
            newPosition.z = allowedArea.yMax;
            velocity.z = -velocity.z * bouncingness;
        }
        transform.localPosition =newPosition;
    }
}
